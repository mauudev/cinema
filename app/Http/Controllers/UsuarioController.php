<?php

namespace Cinema\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use Illuminate\Routing\Route;
use Cinema\Http\Controllers\Controller;
use Cinema\Http\Requests;
use Cinema\Http\Requests\UserCreateRequest;
use Cinema\Http\Requests\UserUpdateRequest;
use Cinema\User;
use Redirect;


//use Illuminate\Routing\Route;
//use Laracasts\Flash\Flash;

class UsuarioController extends Controller
{
    public function __construct(){
      $this->middleware('auth');
      $this->middleware('admin',['only'=>['edit','update','destroy']]);

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
       //$users = User::All();
       $users = User::paginate(5);
       if($request->ajax()){
         return response()->json(view('usuario.users',compact('users'))->render());
       }
       //para ver solo los dados de baja - revisar User.php y su migracion correspondiente
       //$users = User::onlyTrashed()->paginate(5);
       return view('usuario.index',compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('usuario.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserCreateRequest $request)
    {
        // $user = new User($request->all());
        // $user->password = bcrypt($request->password);
        // $user->save();
        User::create($request->all());
        Session::flash('store_message','Usuario agregado correctamente!');
        //return redirect('/usuario')->with('message', 'store');
        return redirect()->route('usuario.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       $user = User::find($id);
       //return view('usuario.edit',['user'=>$user]);
       return view('usuario.edit')->with('user', $user);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UserUpdateRequest $request, $id)
    {
      $user = User::find($id);
      $user->name = $request->name;
      $user->email = $request->email;
      $user->save();
      Session::flash('update_message','El usuario '.$user->name.' ha sido editado correctamente!');
      return redirect()->route('usuario.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);
        //User::destroy($id);
        $user->delete();
        Session::flash('update_message','El usuario ha sido eliminado correctamente!');
        //Flash::danger('El usuario '.$user->name.' ha sido eliminado de forma exitosa!'); PROBAR LARACASTS
        return redirect()->route('usuario.index');
    }
}
