<?php

namespace Cinema\Http\Controllers;

use Illuminate\Http\Request;

use Cinema\Http\Requests;
use Auth;
use Redirect;
use Session;
use Cinema\Http\Requests\LoginRequest;

class LoginController extends Controller
{
  public function login(LoginRequest $request)
  {
      if(Auth::attempt(['email'=>$request['email'],'password'=>$request['password']]))
        return Redirect::to('admin');
      else{
         Session::Flash('message_error','El e-mail o password son incorrectos');
         return Redirect::to('/');
       }
  }
  public function logout(){
    Auth::logout();
    return Redirect::to('/');
  }
}
