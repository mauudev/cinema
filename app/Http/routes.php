<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|post,get,put,delete
*/
//EJEMPLOS DE RUTAS:
// Route::get('prueba', function (){
//   return "Hola desde routes.php";
// });
// Route::get('nombre/{nombre}',function($nombre){
//   return "Mi nombre es: ".$nombre;
// });
// Route::get('edad/{edad?}',function($edad = 26){
//   return "Mi edad es: ".$edad;
// });
// Route::get('/', function () {
//     return view('welcome');
// });
// Route::get('controlador','PruebaController@index');//@index hace referencia al metodo index del controlador
// Route::get('name/{nombre}','PruebaController@nombre');
// Route::resource('movie','MovieController');
Route::get('/','FrontController@index');
Route::get('contact','FrontController@contact');
Route::get('reviews','FrontController@reviews');
Route::get('admin','FrontController@admin');

Route::resource('usuario','UsuarioController');
Route::get('usuario/{usuario}/destroy',[
  'uses'=>'UsuarioController@destroy',
  'as' => 'usuario.destroy'
]);
Route::resource('genero','GeneroController');
Route::resource('pelicula','MovieController');
Route::get('pelicula/{pelicula}/destroy',[
  'uses'=>'MovieController@destroy',
  'as' => 'pelicula.destroy'
]);
//Route::resource('login','LogController');
Route::post('login',['uses'=>'LoginController@login','as'=>'login.login']);
Route::get('logout',['uses'=>'LoginController@logout','as'=>'login.logout']);//*
