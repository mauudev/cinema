public function up()
{
  Schema::create('genres',function(Blueprint $table){
    $table->increments('id');
    $table->string('genre');
    $table->timestamps();
  });
}



public function up()
{
    Schema::create('movies',function(Blueprint $table){
      $table->increments('id');
      $table->string('name');
      $table->string('cast');
      $table->string('direction');
      $table->string('duration');
      $table->timestamps();
      //llave foranea a la tabla genres
      $table->integer('genre_id')->unsigned();
      $table->foreign('genre_id')->references('id')->on('genres');
    });
}
