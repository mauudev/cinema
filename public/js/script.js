$("#registro").click(function(){
  var dato = $("#genre").val();
  var route = "http://localhost:8000/genero";
  var token = $("#token").val();
  $.ajax({
    url: route,
    headers: {'X-CSRF-TOKEN': token},
    type: 'POST',
    dataType: 'json',
    data: {genre: dato},

    success:function(){
      // var loc = window.location;
      // window.location = loc.protocol+"//"+loc.hostname+":8000/genero";
      //$("#msj-success").fadeIn();
      window.location.replace('/genero');
    },
    error:function(msj){
      //console.log(msj.responseJSON.genre);
      $("#msj").html(msj.responseJSON.genre);
      $("#msj-error").fadeIn();
    }
  });
});
