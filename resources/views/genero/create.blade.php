@extends('layouts.admin')
@section('content')
  {!!Form::open()!!}
  <div id="msj-success" class="alert alert-success alert-dismissible" role="alert" style="display:none">
    <strong>Genero agregado correctamente !</strong>
  </div>
  <div id="msj-error" class="alert alert-danger alert-dismissible" role="alert" style="display:none">
    <strong id="msj">Genero agregado correctamente !</strong>
  </div>
  <input type="hidden" name="_token" value="{{ csrf_token()}}" id="token">
  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div class="box-header">
            <h3 class="box-title">Usuarios</h3>
          </div>
          <!-- /.box-header -->
          <div class="box-body">
    @include('genero.form.genero')
    {!!link_to('#',$title='Registrar',$attributes=['id'=>'registro','class'=>'btn btn-primary'],$secure=null)!!}
  </div>
  </div>
  </div>
  </div>
  </div>
  </section>
  </div>
  {!!Form::close()!!}
@endsection
@section('scripts')
  {!!Html::script('js/jquery-2.0.3.min.js')!!}
  {!!Html::script('js/script.js')!!}
@endsection
