@extends('layouts.admin')
	@section('content')
  @include('genero.modal')
	@include('alerts.success')
	<div id="msj-success" class="alert alert-success alert-dismissible" role="alert" style="display:none">
  		<strong> Genero Actualizado Correctamente.</strong>
	</div>
	<div class="users">
	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box">
					<div class="box-header">
						<h3 class="box-title">Generos</h3>
					</div>
					<!-- /.box-header -->
					<div class="box-body">
		<table class="table">
			<thead>
				<th>Nombre</th>
				<th>Operaciones</th>
			</thead>
			<tbody id="datos">
				{{-- rellenar desde ajax --}}
			</tbody>
		</table>
	</div>
	</div>
</div>
</div>
	</section>
	</div>
{{-- {!!$genres->render()!!} --}}

@endsection
@section('scripts')
	{!!Html::script('js/jquery-2.0.3.min.js')!!}
  {!!Html::script('js/script2.js')!!}
@endsection
