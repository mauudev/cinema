<section class="content-header">
  @if(Session::has('store_message'))
    <div class="alert alert-success" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Exito!</strong> {!! session('store_message') !!}
    </div>
  @elseif(Session::has('update_message'))
    <div class="alert alert-success" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Exito!</strong> {!! session('update_message') !!}
    </div>
  @elseif(Session::has('msj-success'))
    <div class="alert alert-success" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Exito!</strong> {!! session('msj-success') !!}
    </div>
  @endif
</section>
