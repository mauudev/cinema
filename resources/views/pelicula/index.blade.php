@extends('layouts.admin')
	@include('alerts.success')
	@section('content')
		<div class="users">
		<section class="content">
			<div class="row">
				<div class="col-xs-12">
					<div class="box">
						<div class="box-header">
							<h3 class="box-title">Peliculas</h3>
						</div>
						<!-- /.box-header -->
						<div class="box-body">
		<table class="table">
			<thead>
				<th>Nombre</th>
				<th>Genero</th>
				<th>Direccion</th>
				<th>Caratula</th>
				<th>Operaciones</th>
			</thead>
			@foreach($movies as $movie)
				<tbody>
					<td>{{$movie->name}}</td>
					<td>{{$movie->genre}}</td>
					<td>{{$movie->direction}}</td>
					<td>
						<img src="movies/{{$movie->path}}" alt="" style="width:100px;"/>
					</td>
					<td>{!!link_to_route('pelicula.edit', $title = 'Editar', $parameters = $movie->id, $attributes = ['class'=>'btn btn-primary'])!!}
					{!!link_to_route('pelicula.destroy',$title = 'Eliminar',$parameteres = $movie->id,$attributes = ['class'=>'btn btn-danger','onclick'=>'return confirm("¿Estas seguro de querer eliminar?")'])!!}</td></td>
				</tbody>
			@endforeach
		</table>
	</div>
	</div>
</div>
</div>
	</section>
	</div>
	@endsection
