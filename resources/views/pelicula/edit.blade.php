@extends('layouts.admin')
	@section('content')
		@include('alerts.required')
		
		<div class="users">
		<section class="content">
			<div class="row">
				<div class="col-xs-12">
					<div class="box">
						<div class="box-header">
							<h3 class="box-title">Pelicula</h3>
						</div>
						<!-- /.box-header -->
						<div class="box-body">
		{!!Form::model($movie,['route'=> ['pelicula.update',$movie->id],'method'=>'PUT','files' => true])!!}
			@include('pelicula.forms.pelicula')
			{!!Form::submit('Actualizar',['class'=>'btn btn-primary'])!!}
		{!!Form::close()!!}

		{!!Form::open(['route'=> ['pelicula.destroy',$movie->id],'method'=>'DELETE'])!!}
			{!!Form::submit('Eliminar',['class'=>'btn btn-danger'])!!}
		{!!Form::close()!!}
	</div>
	</div>
</div>
</div>
	</section>
	</div>
	@endsection
