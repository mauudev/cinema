@extends('layouts.admin')
	@section('content')
		@include('alerts.required')
		  	{!!Form::open(['route'=>'pelicula.store', 'method'=>'POST','files' => true])!!}
				<div class="users">
				<section class="content">
				  <div class="row">
				    <div class="col-xs-12">
				      <div class="box">
				        <div class="box-header">
				          <h3 class="box-title">Pelicula</h3>
				        </div>
				        <!-- /.box-header -->
				        <div class="box-body">
		  		@include('pelicula.forms.pelicula')
				{!!Form::submit('Registrar',['class'=>'btn btn-primary'])!!}
			</div>
			</div>
		</div>
		</div>
			</section>
			</div>
			{!!Form::close()!!}
	@endsection
