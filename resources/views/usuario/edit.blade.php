@extends('layouts.admin')
@section('content')
@include('alerts.required')
          {!!Form::model($user,['route'=>['usuario.update',$user->id],'method'=>'PUT'])!!} <!--enrutando los datos -->
          <div class="row">
              <div class="col-md-6">
                  <div class="box box-primary">
                      <div class="box-header with-border">
                          <h3 class="box-title">Actualizar datos de usuario</h3>
                      </div>
                      <form role="form">
                          <div class="box-body">
                        @include('usuario.forms.user')
                      {!!Form::submit('Actualizar',['class'=>'btn btn-primary'])!!}
                      {!!Form::close()!!}
                      {{-- {!!Form::open(['route'=>['usuario.destroy',$user->id],'method'=>'DELETE'])!!}
                      {!!Form::submit('Eliminar',['class'=>'btn btn-danger'])!!}
                      {!!Form::close()!!} --}}
                      </div>
                      </form>

                </div>
            </div>
        </div>
      </form>
      </div>
      </div>
      </div>


@endsection
@section('scripts')
  {!!Html::script('js/jquery-2.0.3.min.js')!!}
  {!!Html::script('js/script3.js')!!}
@endsection
