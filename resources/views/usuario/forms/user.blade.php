<div class="form-group">
      {!!Form::label('Nombre:')!!}
      {!!Form::text('name',null,['class'=>'form-control','placeholder'=>'Ingresa el nombre de usuario'])!!}
    </div>
    <div class="form-group">
      {!!Form::label('Email:')!!}
      {!!Form::text('email',null,['class'=>'form-control','placeholder'=>'Ingresa tu email'])!!}
    </div>
    @if(Route::current()->getName() == 'usuario.edit')
    @else
      <div class="form-group">
        {!!Form::label('Password:')!!}
        {!!Form::password('password',['class'=>'form-control','placeholder'=>'Ingresa tu password'])!!}
    @endif
    </div>
