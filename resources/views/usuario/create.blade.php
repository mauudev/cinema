@extends('layouts.admin')
@section('content')
@include('alerts.required')
    {!!Form::open(['route'=>'usuario.store','method'=>'POST'])!!} <!--enrutando los datos -->
    <section class="content">
			<div class="row">
				<div class="col-xs-12">
					<div class="box">
						<div class="box-header">
							<h3 class="box-title">Agregar nuevo usuario</h3>
						</div>
                <form role="form">
                    <div class="box-body">
                  @include('usuario.forms.user')
                {!!Form::submit('Registrar',['class'=>'btn btn-primary'])!!}
                </form>
              </div>
              </div>
              </div>
              </div>
              </section>
              </div>
        {!!Form::close()!!}
@endsection
@section('scripts')
  {!!Html::script('js/jquery-2.0.3.min.js')!!}
  {!!Html::script('js/script3.js')!!}
@endsection
