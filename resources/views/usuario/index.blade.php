@extends('layouts.admin')
@section('content')
@include('alerts.success')
<!-- Main content -->
<div class="users">
<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header">
          <h3 class="box-title">Usuarios</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <table id="example2" class="table table-bordered table-hover">
            <thead>
            <tr>
              <th>Nombre</th>
              <th>Email</th>
              <th>Accion</th>
            </tr>
            </thead>
            @foreach($users as $user)
            <tbody>
            <tr>
              <td>{{$user->name}}</td>
              <td>{{$user->email}}</td>
              <td>{!!link_to_route('usuario.edit', $title = 'Editar', $parameters = $user->id, $attributes = ['class'=>'btn btn-primary'])!!}
              {!!link_to_route('usuario.destroy',$title = 'Eliminar',$parameteres = $user->id,$attributes = ['class'=>'btn btn-danger','onclick'=>'return confirm("¿Estas seguro de querer eliminar?")'])!!}</td>
            </tr>
          </tbody>
          @endforeach
          </table>
          {!!$users->render()!!}
        </div>
      	</div>
      </div>
      </div>
      	</section>
      	</div>
@endsection

@section('scripts')
  {!!Html::script('js/jquery-2.0.3.min.js')!!}
  {!!Html::script('js/script3.js')!!}
@endsection
