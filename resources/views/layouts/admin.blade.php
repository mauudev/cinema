<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Cinema Admin</title>
  <!-- Tell the browser to be responsive to screen width -->

  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  {!!Html::style('bootstrap/css/bootstrap.min.css')!!}
  <!-- Font Awesome -->
  {!!Html::style('dist/css/font-awesome.min.css')!!}
  <!-- Ionicons -->
  {!!Html::style('dist/css/ionicons.min.css')!!}
  <!-- Theme style -->
  {!!Html::style('dist/css/AdminLTE.min.css')!!}
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  {!!Html::style('dist/css/skins/_all-skins.min.css')!!}
  <!-- iCheck -->
  {!!Html::style('plugins/iCheck/flat/blue.css')!!}
  <!-- Morris chart -->
  {!!Html::style('plugins/morris/morris.css')!!}
  <!-- jvectormap -->
  {!!Html::style('plugins/jvectormap/jquery-jvectormap-1.2.2.css')!!}
  <!-- Date Picker -->
  {!!Html::style('plugins/datepicker/datepicker3.css')!!}
  <!-- Daterange picker -->
  {!!Html::style('plugins/daterangepicker/daterangepicker-bs3.css')!!}
  <!-- bootstrap wysihtml5 - text editor -->
  {!!Html::style('plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css')!!}
  {!!Html::style('plugins/datatables/dataTables.bootstrap.css')!!}

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  {!!Html::script('https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js')!!}
  {!!Html::script('https://oss.maxcdn.com/respond/1.4.2/respond.min.js')!!}
  <![endif]-->
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <header class="main-header">
    <!-- Logo -->
    <a href="index2.html" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>Cinema</b>Admin</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>Cinema Admin</b></span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- Messages: style can be found in dropdown.less-->
          <!-- Notifications: style can be found in dropdown.less -->
          <!-- Tasks: style can be found in dropdown.less -->

          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              {{ Html::image('dist/img/user2-160x160.jpg', 'User Image', array('class' => 'user-image')) }}
              <span class="hidden-xs">{!!Auth::user()->name!!}</span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                {{ Html::image('dist/img/user2-160x160.jpg', 'User Image', array('class' => 'img-circle')) }}

              </li>
              <!-- Menu Body -->
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-left">
                  <a href="#" class="btn btn-default btn-flat">Profile</a>
                </div>
                <div class="pull-right">
                  <a href="/logout" class="btn btn-default btn-flat">Sign out</a>
                </div>
              </li>
            </ul>
          </li>
          <!-- Control Sidebar Toggle Button -->

        </ul>
      </div>
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          {{ Html::image('dist/img/user2-160x160.jpg', 'User Image', array('class' => 'img-circle')) }}
        </div>
        <div class="pull-left info">
          <p>{!!Auth::user()->name!!}</p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <!-- search form -->
      <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
              <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
        </div>
      </form>
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu">
        <li class="header">MAIN NAVIGATION</li>
        @if(Auth::user()->id == 1)
        <li class="active treeview">
          <a href="#">
            <i class="fa fa-dashboard"></i> <span>Usuarios</span> <i class="fa fa-angle-left pull-right"></i>
          </a>
          <ul class="treeview-menu">
            <li class="active"><a href="{!!URL::to('/usuario/create')!!}"><i class="fa fa-circle-o"></i> Agregar</a></li>
            <li><a href="{!!URL::to('/usuario')!!}"><i class="fa fa-circle-o"></i> Usuarios</a></li>
          </ul>
        </li>
      @endif
		<li class="active treeview">
          <a href="#">
            <i class="fa fa-dashboard"></i> <span>Peliculas</span> <i class="fa fa-angle-left pull-right"></i>
          </a>
          <ul class="treeview-menu">
            <li class="active"><a href="{!!URL::to('/pelicula/create')!!}"><i class="fa fa-circle-o"></i> Agregar</a></li>
            <li><a href="{!!URL::to('/pelicula')!!}"><i class="fa fa-circle-o"></i> Peliculas</a></li>
          </ul>
        </li>
		<li class="active treeview">
          <a href="#">
            <i class="fa fa-dashboard"></i> <span>Generos</span> <i class="fa fa-angle-left pull-right"></i>
          </a>
          <ul class="treeview-menu">
            <li class="active"><a href="{!!URL::to('/genero/create')!!}"><i class="fa fa-circle-o"></i> Agregar</a></li>
            <li><a href="{!!URL::to('/genero')!!}"><i class="fa fa-circle-o"></i> Generos</a></li>
          </ul>
        </li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content -->
    @yield('content')

	<!-- Content -->
  </div>
</div>
  <!-- /.content-wrapper -->


  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Create the tabs -->
    <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
      <li><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li>
      <li><a href="#control-sidebar-settings-tab" data-toggle="tab"><i class="fa fa-gears"></i></a></li>
    </ul>
    <!-- Tab panes -->
      <!-- /.tab-pane -->
      <!-- Stats tab content -->
      <div class="tab-pane" id="control-sidebar-stats-tab">Stats Tab Content</div>
      <!-- /.tab-pane -->

      <!-- /.tab-pane -->
    </div>
  </aside>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>

<!-- ./wrapper -->

<!-- jQuery 2.2.0 -->
{{-- {!!Html::script('plugins/jQuery/jQuery-2.2.0.min.js')!!} --}}
<!-- jQuery UI 1.11.4 -->
@section('scripts')
@show

{!!Html::script('plugins/jQuery/jquery-ui.min.js')!!}
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>

<!-- Bootstrap 3.3.6 -->
{!!Html::script('bootstrap/js/bootstrap.min.js')!!}
<!-- Morris.js charts -->
{!!Html::script('plugins/jQuery/raphael-min.js')!!}
{!!Html::script('plugins/morris/morris.min.js')!!}
<!-- Sparkline -->
{!!Html::script('plugins/sparkline/jquery.sparkline.min.js')!!}
<!-- jvectormap -->
{!!Html::script('plugins/jvectormap/jquery-jvectormap-1.2.2.min.js')!!}
{!!Html::script('plugins/jvectormap/jquery-jvectormap-world-mill-en.js')!!}
<!-- jQuery Knob Chart -->
{!!Html::script('plugins/knob/jquery.knob.js')!!}
<!-- daterangepicker -->
{!!Html::script('plugins/jQuery/moment.min.js')!!}
{!!Html::script('plugins/daterangepicker/daterangepicker.js')!!}
<!-- datepicker -->
{!!Html::script('plugins/datepicker/bootstrap-datepicker.js')!!}
<!-- Bootstrap WYSIHTML5 -->
{!!Html::script('plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js')!!}
<!-- Slimscroll -->
{!!Html::script('plugins/slimScroll/jquery.slimscroll.min.js')!!}
<!-- FastClick -->
{!!Html::script('plugins/fastclick/fastclick.js')!!}
<!-- AdminLTE App -->
{!!Html::script('dist/js/app.min.js')!!}
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
{!!Html::script('dist/js/pages/dashboard.js')!!}
<!-- AdminLTE for demo purposes -->
{!!Html::script('dist/js/demo.js')!!}




</body>
</html>
